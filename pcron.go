package main

import (
	"fmt"
	"os"
    "log"
	"bufio"
	"time"
	"sync"
)

type Cron []CronJob

// Self explanetory. Type for jobs. "runs job every x y z"
// prev and next point to the last time the job was run and the next time it will run, respectively
type CronJob struct {
	secs, mins, hours, days uint
	job string
}

func CronTabReader(ctabfile string) ([]string, error) {
	crontab, err := os.Open(ctabfile)
	if err != nil {
        return nil, err
    }
	defer crontab.Close()

	var lines []string
	scanner := bufio.NewScanner(crontab)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return lines, scanner.Err()
}

func JobParser(crontab []string) []CronJob {
	// Create CronJob for each line on the CronTab
	var jobs []CronJob
	var cj CronJob

	for _, tab := range crontab {
		fmt.Sscanf(
			tab,
			"%d %d %d %d %s",
			&cj.secs,
			&cj.mins,
			&cj.hours,
			&cj.days,
			&cj.job)
		jobs = append(jobs, cj)
	}

	return jobs
}

// Cron Methods
func (c Cron) Start() {
	// Here we start the Cron scheduler
	now := time.Now().Local()
	var wg sync.WaitGroup

	for _, jobs := range c {
		wg.Add(1)
		go jobs.ScheduleTask(now, &wg)
	}

	wg.Wait()
}

func main() {
	ctabfile := "pcrontab" //temporary

	crontab, err := CronTabReader(ctabfile)
	if err != nil {
		log.Fatalf("CronTabReader: %s", err)
	}

	cronjobs := JobParser(crontab)

	//debugging
	for _, job := range cronjobs {

		fmt.Printf("%d %d %d %d %s\n", 
			job.secs,
			job.mins,
			job.hours,
			job.days,
			job.job)
	}

	var cron Cron
	cron = cronjobs
	cron.Start()

}