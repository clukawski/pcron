package main

import (
	"time"
	"fmt"
	"os/exec"
	"sync"
)

// CronJob Methods
// Returns the next time the job is activated
func (cj CronJob) Period() time.Duration {
	var sched time.Duration
	sched = time.Duration(cj.secs)*time.Second
	sched += time.Duration(cj.mins)*time.Minute
	sched += time.Duration(cj.hours)*time.Hour
	sched += time.Duration(cj.days*24)*time.Hour

	fmt.Println(sched.String())

	return sched
}

// Scheduling
func (cj CronJob) ScheduleTask(now time.Time, wg *sync.WaitGroup) {
	fmt.Println(now.String())		
	fmt.Println(cj.job)

	ticker := time.NewTicker(cj.Period())
	quit := make(chan struct {})

	for {
		select {
		case <- ticker.C:
			cmd := exec.Command(cj.job, "")
			cmd.Run()
		case <- quit:
			ticker.Stop()
		}
	}

	wg.Done()
}